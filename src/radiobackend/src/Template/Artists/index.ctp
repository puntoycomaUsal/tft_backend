<?php
/**
 * Artists/index Template
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo Artista'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="large-9 medium-8 columns content">
    <h3><?= __('Artistas') ?></h3>
    <table id="dataTable" class="display" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>id</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>F. Nac</th>
                <th>L. Nac.</th>
                <th>Web</th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($datas as $data): ?>
            <tr>
                <td><?= $data->id ?></td>
                <td><?= $data->first_name ?></td>
                <td><?= $data->last_name ?></td>
                <td><?= date('d/m/Y',strtotime($data->birth_date)) ?></td>
                <td><?= $data->birth_place ?></td>
                <td><a href="<?= $data->web ?>" target="_blank">Ir a web </a></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $data->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $data->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estás seguro que quieres borrarlo # {0}?', $data->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable( {
            "pagingType": "full_numbers",
            "responsive": true,
            language: {
                processing:       "<?=__('Procesando registros ...'); ?>",
                search:           "<?=__('Buscar en tabla'); ?>",
                lengthMenu:       "<?=__('Mostrar _MENU_ elementos'); ?>",
                info:             "<?=__('Mostrando  _START_ a _END_ de _TOTAL_ registros'); ?>",
                infoEmpty:        "<?=__('Mostrando  0 a 0 de 0 registros'); ?>",
                infoFiltered:     "<?=__('(Registros filtrados, _MAX_ en total)'); ?>",
                infoPostFix:      "",
                loadingRecords:   "<?=__('Cargando registros ...'); ?>",
                zeroRecords:      "<?=__('No hay datos para mostrar'); ?>",
                emptyTable:       "<?=__('No hay datos disponibles para la los filtros actuales'); ?>",
                paginate: {
                    first:          "<?=__('Primero'); ?>",
                    previous:       "<?=__('Previo'); ?>",
                    next:           "<?=__('Siguiente'); ?>",
                    last:           "<?=__('Último'); ?>"
                },
                aria: {
                    sortAscending:  "<?=__(': activando para ordenar la columna en orden ascendente'); ?>",
                    sortDescending: "<?=__(': activando para ordenar la columna en orden descendente'); ?>",
                }
            }
        } );
    } );
</script>
