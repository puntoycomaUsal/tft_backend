<?php
/**
 * Artists/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista Artistas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('Artists') ?>
    <fieldset>
            <legend><?= __('Nuevo Artista') ?></legend>
            <?php
                echo $this->Form->input('first_name',['label' =>'Nombre']);
                echo $this->Form->input('last_name', ['label' => 'Apellidos']);
                echo $this->Form->label('Fecha de nacimiento');
                //Uso el control de html5 ya que el control de cakephp supone un dolor configurarlo contra la sencillez de html5
                echo '<input id="date" name="birth_date" type="date">';
                echo $this->Form->input('birth_place', ['label' => 'Lugar de nacimiento']);
                echo $this->Form->input('web', ['label' => 'Web', "value" => "http://"]);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
