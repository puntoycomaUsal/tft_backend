<?php
/**
 * Artists/view Template
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Artista'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Artista'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Artistas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Artista'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->first_name.' '.$data->last_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Nombre') ?></th>
            <td><?= h($data->first_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Apellidos') ?></th>
            <td><?= h($data->last_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha de nacimiento') ?></th>
            <td><?= date('d/m/Y',strtotime($data->birth_date)) ?></td>
        </tr>
        <tr>
            <th><?= __('Lugar de nacimiento') ?></th>
            <td><?= h($data->birth_place) ?></td>
        </tr>
        <tr>
            <th><?= __('Web') ?></th>
            <td><a href="<?= $data->web ?>" target="_blank">Ir a web </a></td>
        </tr>
    </table>
</div>
