<?php
/**
 * Artists/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista Artistas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('Artists') ?>
    <fieldset>
            <legend><?= __('Editar Artista') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('first_name',['label' =>'Nombre','value' => $data->first_name]);
                echo $this->Form->input('last_name', ['label' => 'Apellidos','value' => $data->last_name]);
                echo $this->Form->label('Fecha de nacimiento');
                //Uso el control de html5 ya que el control de cakephp supone un dolor configurarlo contra la sencillez de html5
                echo '<input id="date" name="birth_date" type="date" value="'.date('Y-m-d',strtotime($data->birth_date)).'">';
                echo $this->Form->input('birth_place', ['label' => 'Lugar de nacimiento','value' => $data->birth_place]);
                echo $this->Form->input('web', ['label' => 'Web', "value" => $data->web]);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
