<?php
/**
 * Albumcollections/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista Albumcollections'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('Albumcollections') ?>
    <fieldset>
            <legend><?= __('Nuevo Albumcollection') ?></legend>
            <?php
                echo $this->Form->hidden('_id');
                echo $this->Form->input('title', ['label' => 'Titulo']);
                echo $this->Form->input('description', ['label' => 'Descripción']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
