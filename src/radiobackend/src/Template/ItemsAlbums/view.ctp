<?php
/**
 * ItemsAlbums/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar ItemsAlbum'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar ItemsAlbum'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar ItemsAlbums'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo ItemsAlbum'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo de Item') ?></th>
            <td><?= $typeitem_name ?></td>
        </tr>
        <tr>
            <th><?= __('Album') ?></th>
            <td><?= $album_name ?></td>
        </tr>
        <tr>
            <th><?= __('Item') ?></th>
            <td><?= $item_name ?></td>
        </tr>
        <tr>
            <th><?= __('Posición') ?></th>
            <td><?= $this->Number->format($data->position) ?></td>
        </tr>
    </table>
</div>
