<?php
/**
 * ItemsAlbums/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista ItemsAlbums'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ItemsAlbums form large-9 medium-8 columns content">
    <?= $this->Form->create('ItemsAlbums') ?>
    <fieldset>
            <legend><?= __('Editar ItemsAlbum') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('album_id',[ 'type' => 'select','label' => 'Album', 'options' => $AlbumsOptions,'value' => $data->album_id]);
                echo $this->Form->input('item_id',[ 'type' => 'select','label' => 'Item', 'options' => $ItemsOptions,'value' => $data->item_id]);
                echo $this->Form->label('Posición');
                echo '<input id="number" name="position" type="number" value="'.$data->position.'">';
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
