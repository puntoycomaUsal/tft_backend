<?php
/**
 * Typeitems/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista Typeitems'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="Typeitems form large-9 medium-8 columns content">
    <?= $this->Form->create('Typeitems') ?>
    <fieldset>
            <legend><?= __('Nuevo Typeitem') ?></legend>
            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
