<?php
/**
 * Pages/home Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>

    <h3><?= __('Listado Modelos Disponibles ') ?></h3>
    <table id="dataTable" class="display" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>modelo</th>
                <th>urlApi</th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($endpoints as $model => $url): ?>
                <tr>
                    <td><?= $model ?></td>
                    <td><?= $url ?></td>
                    <td class="actions">
                    <?php $linkToModelIndex=$this->Url->build([
                        "controller" => $model,
                        "action"     => "index",
                    ]); ?>
                    <?= $this->Html->link(__('Ir al modelo'), $linkToModelIndex) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable( {
            "pagingType": "full_numbers",
            "responsive": true,
            language: {
                processing:       "<?=__('Procesando registros ...'); ?>",
                search:           "<?=__('Buscar en tabla'); ?>",
                lengthMenu:       "<?=__('Mostrar _MENU_ elementos'); ?>",
                info:             "<?=__('Mostrando  _START_ a _END_ de _TOTAL_ registros'); ?>",
                infoEmpty:        "<?=__('Mostrando  0 a 0 de 0 registros'); ?>",
                infoFiltered:     "<?=__('(Registros filtrados, _MAX_ en total)'); ?>",
                infoPostFix:      "",
                loadingRecords:   "<?=__('Cargando registros ...'); ?>",
                zeroRecords:      "<?=__('No hay datos para mostrar'); ?>",
                emptyTable:       "<?=__('No hay datos disponibles para la los filtros actuales'); ?>",
                paginate: {
                    first:          "<?=__('Primero'); ?>",
                    previous:       "<?=__('Previo'); ?>",
                    next:           "<?=__('Siguiente'); ?>",
                    last:           "<?=__('Último'); ?>"
                },
                aria: {
                    sortAscending:  "<?=__(': activando para ordenar la columna en orden ascendente'); ?>",
                    sortDescending: "<?=__(': activando para ordenar la columna en orden descendente'); ?>",
                }
            }
        } );
    } );
</script>
