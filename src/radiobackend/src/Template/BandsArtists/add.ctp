<?php
/**
 * BandsArtists/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista BandsArtistas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('BandsArtists') ?>
    <fieldset>
            <legend><?= __('Nuevo BandaArtista') ?></legend>
            <?php
                echo $this->Form->input('artist_id',[ 'type' => 'select','label' => 'Artist', 'options' => $ArtistsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('band_id',[ 'type' => 'select','label' => 'Band', 'options' => $BandsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('role_id',[ 'type' => 'select','label' => 'Role', 'options' => $RolesOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->label('Fecha inicio');
                echo '<input id="date" name="start_date" type="date">';
                echo $this->Form->label('Fecha fin');
                echo '<input id="date" name="end_date" type="date">';
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
