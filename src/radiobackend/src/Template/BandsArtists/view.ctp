<?php
/**
 * BandsArtists/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar BandasArtista'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar BandasArtista'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar BandasArtistas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo BandasArtista'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Artist_Id') ?></th>
            <td><?= $artist_name ?></td>
        </tr>
        <tr>
            <th><?= __('Band_Id') ?></th>
            <td><?= $band_name?></td>
        </tr>
        <tr>
            <th><?= __('Role_Id') ?></th>
            <td><?= $role_name ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha inicio') ?></th>
            <td><?= date('d/m/Y',strtotime($data->start_date)) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha fin') ?></th>
            <td><?= date('d/m/Y',strtotime($data->end_date)) ?></td>
        </tr>
    </table>
</div>
