<?php
/**
 * BandsArtists/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista BandsArtistas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('BandsArtists') ?>
    <fieldset>
            <legend><?= __('Editar BandsArtista') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('artist_id',[ 'type' => 'select','label' => 'Artist', 'options' => $ArtistsOptions,'value' => $data->artist_id]);
                echo $this->Form->input('band_id',[ 'type' => 'select','label' => 'Band', 'options' => $BandsOptions,'value' => $data->band_id]);
                echo $this->Form->input('role_id',[ 'type' => 'select','label' => 'Role', 'options' => $RolesOptions,'value' => $data->role_id]);
                echo $this->Form->label('Fecha inicio');
                echo '<input id="date" name="start_date" type="date" value="'.date('Y-m-d',strtotime($data->start_date)).'">';
                echo $this->Form->label('Fecha fin');
                echo '<input id="date" name="end_date" type="date" value="'.date('Y-m-d',strtotime($data->end_date)).'">';
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
