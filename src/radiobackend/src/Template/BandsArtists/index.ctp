<?php
/**
 * BandsArtists/index Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo BandaArtista'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="large-9 medium-8 columns content">
    <h3><?= __('BandasArtistas') ?></h3>
    <table id="dataTable" class="display" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>id</th>
                <th>Artista Id</th>
                <th>Banda Id</th>
                <th>Rol Id</th>
                <th>F. Inicio</th>
                <th>F. Fin</th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($datas as $data): ?>
            <tr>
                <td><?= $data->id ?></td>
                <td><?= $Artists->getRecord($data->artist_id)->getDataFirst()->first_name?></td>
                <td><?= $Bands->getRecord($data->band_id)->getDataFirst()->name?></td>
                <td><?= $Roles->getRecord($data->role_id)->getDataFirst()->name?></td>
                <td><?= date('d/m/Y',strtotime($data->start_date)) ?></td>
                <td><?= date('d/m/Y',strtotime($data->end_date)) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $data->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $data->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estás seguro que quieres borrarlo # {0}?', $data->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable( {
            "pagingType": "full_numbers",
            "responsive": true,
            language: {
                processing:       "<?=__('Procesando registros ...'); ?>",
                search:           "<?=__('Buscar en tabla'); ?>",
                lengthMenu:       "<?=__('Mostrar _MENU_ elementos'); ?>",
                info:             "<?=__('Mostrando  _START_ a _END_ de _TOTAL_ registros'); ?>",
                infoEmpty:        "<?=__('Mostrando  0 a 0 de 0 registros'); ?>",
                infoFiltered:     "<?=__('(Registros filtrados, _MAX_ en total)'); ?>",
                infoPostFix:      "",
                loadingRecords:   "<?=__('Cargando registros ...'); ?>",
                zeroRecords:      "<?=__('No hay datos para mostrar'); ?>",
                emptyTable:       "<?=__('No hay datos disponibles para la los filtros actuales'); ?>",
                paginate: {
                    first:          "<?=__('Primero'); ?>",
                    previous:       "<?=__('Previo'); ?>",
                    next:           "<?=__('Siguiente'); ?>",
                    last:           "<?=__('Último'); ?>"
                },
                aria: {
                    sortAscending:  "<?=__(': activando para ordenar la columna en orden ascendente'); ?>",
                    sortDescending: "<?=__(': activando para ordenar la columna en orden descendente'); ?>",
                }
            }
        } );
    } );
</script>
