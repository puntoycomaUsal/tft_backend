<?php
/**
 * Newcollections/index Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo Newcollection'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="large-9 medium-8 columns content">
    <h3><?= __('Newcollections') ?></h3>
    <table id="dataTable" class="display" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Id</th>
                <th>Titulo</th>
                <th>Fecha</th>
                <th>Descripción</th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($datas as $data): ?>
            <tr>
                <td><?= $data->_id ?></td>
                <td><?= $data->title ?></td>
                <td><?= $data->date ?></td>
                <td><?= $data->description ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $data->_id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $data->_id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $data->_id], ['confirm' => __('¿Estás seguro que quieres borrarlo # {0}?', $data->_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable( {
            "pagingType": "full_numbers",
            "responsive": true,
            language: {
                processing:       "<?=__('Procesando registros ...'); ?>",
                search:           "<?=__('Buscar en tabla'); ?>",
                lengthMenu:       "<?=__('Mostrar _MENU_ elementos'); ?>",
                info:             "<?=__('Mostrando  _START_ a _END_ de _TOTAL_ registros'); ?>",
                infoEmpty:        "<?=__('Mostrando  0 a 0 de 0 registros'); ?>",
                infoFiltered:     "<?=__('(Registros filtrados, _MAX_ en total)'); ?>",
                infoPostFix:      "",
                loadingRecords:   "<?=__('Cargando registros ...'); ?>",
                zeroRecords:      "<?=__('No hay datos para mostrar'); ?>",
                emptyTable:       "<?=__('No hay datos disponibles para la los filtros actuales'); ?>",
                paginate: {
                    first:          "<?=__('Primero'); ?>",
                    previous:       "<?=__('Previo'); ?>",
                    next:           "<?=__('Siguiente'); ?>",
                    last:           "<?=__('Último'); ?>"
                },
                aria: {
                    sortAscending:  "<?=__(': activando para ordenar la columna en orden ascendente'); ?>",
                    sortDescending: "<?=__(': activando para ordenar la columna en orden descendente'); ?>",
                }
            }
        } );
    } );
</script>
