<?php
/**
 * Newcollections/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Newcollection'), ['action' => 'edit', $data->_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Newcollection'), ['action' => 'delete', $data->_id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->_id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Newcollections'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Newcollection'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Titulo') ?></th>
            <td><?= h($data->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha') ?></th>
            <td><?= date('d/m/Y',strtotime($data->date)) ?></td>
        </tr>
        <tr>
            <th><?= __('Descripción') ?></th>
            <td><?= h($data->description) ?></td>
        </tr>
    </table>
</div>
