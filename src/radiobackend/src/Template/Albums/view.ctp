<?php
/**
 * Albums/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Album'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Album'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Albums'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Album'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha de publicación') ?></th>
            <td><?= date('d/m/Y',strtotime($data->publication_date)) ?></td>
        </tr>
        <tr>
            <th><?= __('Nombre') ?></th>
            <td><?= h($data->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Web') ?></th>
            <td><a href="<?= $data->web ?>" target="_blank">Ir a web </a></td>
        </tr>
    </table>
</div>
