<?php
/**
 * Albums/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista Albums'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('Albums') ?>
    <fieldset>
            <legend><?= __('Editar Album') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('name', ['label' => 'Nombre','value' => $data->name]);
                echo $this->Form->label('Fecha de publicación');
                echo '<input id="date" name="publication_date" type="date" value="'.date('Y-m-d',strtotime($data->publication_date)).'">';
                echo $this->Form->input('web', ['label' => 'Web', "value" => $data->web]);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
