<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Backend para Radio Punto y Coma';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('jquery.dataTables.min.css') ?>
    <?= $this->Html->css('puntoycoma.css') ?>

    <?= $this->Html->script('jquery-3.3.1.js') ?>
    <?= $this->Html->script('jquery.dataTables.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div class="app-container">
        <header class="app-header">
                <div class="app-header-row-1">
                    <?= $this->Html->image('logo.png', ['alt' => 'Blog Punto & Coma']); ?>
                </div>
                <div class="app-header-row-2">
                    <?= $this->Flash->render() ?>
                </div>
        </header>
        <br>
        <section class="app-section">
            <?= $this->fetch('content') ?>
        </section>
        <footer class="app-footer">
            <hr>
            <p><b>Proyecto final Usal, "Experto en Desarrollo FullStack", Front-end</b></p>
            <p>Desarrollado por: Alexander Vicente | Juan Carlos Frutos | Maritza M. Bravo | Jesús M. Nieto </p>
        </footer>
    </div>
</body>
</html>
