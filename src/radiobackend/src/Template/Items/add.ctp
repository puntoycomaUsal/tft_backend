<?php
/**
 * Items/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista Items'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="items form large-9 medium-8 columns content">
    <?= $this->Form->create('Items', ['type' => 'file']) ?>
    <fieldset>
            <legend><?= __('Nuevo Item') ?></legend>
            <?php
                echo $this->Form->input('typeitem_id',[ 'type' => 'select','label' => 'Tipo de item', 'options' => $TypeitemsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('band_id',[ 'type' => 'select','label' => 'Id Banda', 'options' => $BandsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('url', ['type' => 'file']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
