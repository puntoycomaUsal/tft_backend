<?php
/**
 * Items/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Item'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Item'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Item'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo de Item') ?></th>
            <td><?= $typeitem_name ?></td>
        </tr>
        <tr>
            <th><?= __('Nombre') ?></th>
            <td><?= h($data->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Banda') ?></th>
            <td><?= $band_name ?></td>
        </tr>
        <tr>
            <th><?= __('Url') ?></th>
            <td><a href="<?= $data->url ?>" target="_blank">Ir a url </a></td>
        </tr>
    </table>
</div>
