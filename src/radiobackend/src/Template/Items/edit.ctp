<?php
/**
 * Items/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista Items'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="Items form large-9 medium-8 columns content">
    <?= $this->Form->create('Items', ['type' => 'file']) ?>
    <fieldset>
            <legend><?= __('Editar Item') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('typeitem_id',[ 'type' => 'select','label' => 'Tipo de item', 'options' => $TypeitemsOptions,'value' => $data->typeitem_id]);
                echo $this->Form->input('name',['label' =>'Nombre','value' => $data->name]);
                echo $this->Form->input('band_id',[ 'type' => 'select','label' => 'Banda', 'options' => $BandsOptions,'value' => $data->band_id]);
                echo $this->Form->input('url', ['label' => 'Url', "value" => $data->url, 'readonly' => 'readonly']);
                echo $this->Form->input('url', ['label' => 'Cambiar url item', "value" => $data->url, 'type' => 'file']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
