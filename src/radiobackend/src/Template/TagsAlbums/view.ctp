<?php
/**
 * TagsAlbums/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar TagsAlbum'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar TagsAlbum'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar TagsAlbums'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo TagsAlbum'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <h3><?= h($data->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($data->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Album') ?></th>
            <td><?= $album_name ?></td>
        </tr>
        <tr>
            <th><?= __('Tag') ?></th>
            <td><?= $tag_name ?></td>
        </tr>
    </table>
</div>
