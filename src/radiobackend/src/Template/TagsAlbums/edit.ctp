<?php
/**
 * TagsAlbums/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista TagsAlbums'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="TagsAlbums form large-9 medium-8 columns content">
    <?= $this->Form->create('TagsAlbums') ?>
    <fieldset>
            <legend><?= __('Editar TagsAlbum') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('album_id',[ 'type' => 'select','label' => 'Album', 'options' => $AlbumsOptions,'value' => $data->album_id]);
                echo $this->Form->input('tag_id',[ 'type' => 'select','label' => 'Tag', 'options' => $TagsOptions,'value' => $data->tag_id]);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
