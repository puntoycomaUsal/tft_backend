<?php
/**
 * TagsBands/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista TagsBandas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('TagsBands') ?>
    <fieldset>
            <legend><?= __('Nueva TagsBanda') ?></legend>
            <?php
                echo $this->Form->input('band_id',[ 'type' => 'select','label' => 'Band', 'options' => $BandsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('tag_id',[ 'type' => 'select','label' => 'Tag', 'options' => $TagsOptions,'empty' => '(Selecciona uno)']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
