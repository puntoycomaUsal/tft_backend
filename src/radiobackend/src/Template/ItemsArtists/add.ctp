<?php
/**
 * ItemsArtists/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista ItemsArtists'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ItemsArtists form large-9 medium-8 columns content">
    <?= $this->Form->create('ItemsArtists') ?>
    <fieldset>
            <legend><?= __('Nuevo ItemsArtist') ?></legend>
            <?php
                echo $this->Form->input('artist_id',[ 'type' => 'select','label' => 'Artist', 'options' => $ArtistsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('item_id',[ 'type' => 'select','label' => 'Item', 'options' => $ItemsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->hidden('typeitem_id');
                echo $this->Form->input('position',['label' =>'Posición']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>

