<?php
/**
 * Bands/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista Bandas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bands form large-9 medium-8 columns content">
    <?= $this->Form->create('Bands') ?>
    <fieldset>
            <legend><?= __('Nueva Banda') ?></legend>
            <?php
                echo $this->Form->input('typeband_id',[ 'type' => 'select','label' => 'Tipo de banda', 'options' => $TypebandsOptions,'empty' => '(Selecciona uno)']);
                echo $this->Form->input('name',['label' =>'Nombre']);
                echo $this->Form->label('Fecha de fundación');
                echo '<input id="date" name="foundation_date" type="date">';
                echo $this->Form->input('web', ['label' => 'Web', "value" => "http://"]);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
