<?php
/**
 * Configurations/view Template
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Configuración'), ['action' => 'edit', $data->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Configuración'), ['action' => 'delete', $data->id], ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Configuraciones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nueva Configuración'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tasks view large-9 medium-8 columns content">
    <table class="vertical-table">
        <tr>
            <th><?= __('PathResources') ?></th>
            <td><?= h($data->pathResources) ?></td>
        </tr>
        <tr>
            <th><?= __('Resources') ?></th>
            <td><?= h($data->urlResources) ?></td>
        </tr>
        <tr>
            <th><?= __('urlApi') ?></th>
            <td><?= h($data->urlApi) ?></td>
        </tr>
        <tr>
            <th><?= __('endpoints') ?></th>
            <td><?= h($data->endpoints) ?></td>
        </tr>
        <tr>
            <th><?= __('redes sociales') ?></th>
            <td><?= h($data->rrss) ?></td>
        </tr>
    </table>
</div>
