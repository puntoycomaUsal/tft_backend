<?php
/**
 * Configurations/edit Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $data->id],
                ['confirm' => __('¿Estas seguro que quieres borrarlo # {0}?', $data->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista Configuraciones'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('Configurations') ?>
    <fieldset>
            <legend><?= __('Editar Configuracion') ?></legend>
            <?php
                echo $this->Form->hidden('id',['value' => $data->id]);
                echo $this->Form->input('pathResources',['label' =>'Resources','value' => $data->pathResources]);
                echo $this->Form->input('urlResources',['label' =>'Resources','value' => $data->urlResources]);
                echo $this->Form->input('urlApi',['label' =>'urlApi','value' => $data->urlApi]);
                echo $this->Form->input('endpoints',['label' =>'endpoints','value' => $data->endpoints]);
                echo $this->Form->input('rrss',['label' =>'rrss','value' => $data->rrss]);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
