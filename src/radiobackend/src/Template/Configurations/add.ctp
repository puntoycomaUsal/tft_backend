<?php
/**
 * Configurations/add Template
 * Enlace a documentación oficial para controles.
 * https://book.cakephp.org/3.0/en/views/helpers/form.html
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Lista Configuraciones'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create('Configurations') ?>
    <fieldset>
            <legend><?= __('Nueva Configuración') ?></legend>
            <?php
                echo $this->Form->input('pathResources',['label' =>'PathResources']);
                echo $this->Form->input('urlResources',['label' =>'UrlResources']);
                echo $this->Form->input('urlApi',['label' =>'UrlApi']);
                echo $this->Form->input('endpoints',['label' =>'Endpoint']);
                echo $this->Form->input('rrss',['label' =>'Redes Sociales']);
            ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
