<?php
namespace App\Controller;

use App\Controller\AppApisController;
use App\Modelapi\TagsAlbumApi;

/**
 * TagsAlbums Controller
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class TagsAlbumsController extends AppApisController
{

    /**
     * Initialize metodo para inicializar el controlador
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function initialize()
    {
        parent::setConfig(new TagsAlbumApi());
    }

    /**
     * Index método usado para mostrar un listado
     * de registros de la entidad
     *
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function index()
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getList();

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos los datos a mostrar
        $datas              = $modelResponse->getDatas();
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Albums          = $foreings['Albums'];
        $Tags           = $foreings['Tags'];

        $this->set('Albums',$Albums);
        $this->set('Tags',$Tags);
        $this->set(compact('datas'));
        $this->set('_serialize', ['datas']);
        $this->viewBuilder()->layout('default');

    }



    /**
     * Add método para añadir registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se creó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function add()
    {
        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos crear un registro nuevo.

            //Hacemos una consulta contra la api para crear el registro del modelo
            $modelResponse      = $this->model->setRecord($this->request->data);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            switch ($modelResponse->getType()){
                case 1:
                    //correcto
                    $this->Flash->set(__('El registro se guardó correctamente.'), ['element' => 'success']);
                    return $this->redirect(['action' => 'index']);
                break;
                case -1:
                    //error
                    $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                break;
            }

        }
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Tags          = $foreings['Tags'];
        $TagsOptions   = [];
        foreach ($Tags->getList(true)->getDatas() as $data){            
            array_push($TagsOptions,['value' => $data['id'], 'text' => $data['caption']]);
              
        }
        
        $Albums              = $foreings['Albums'];
        $AlbumsOptions       = [];
        foreach ($Albums->getList(true)->getDatas() as $data){            
            array_push($AlbumsOptions,['value' => $data['id'], 'text' => $data['caption']]);
              
        }
       
        $this->set('TagsOptions',$TagsOptions);
        $this->set('AlbumsOptions',$AlbumsOptions);
        $this->viewBuilder()->layout('default');
    }



    /**
     * Add método para editar registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se modificó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function edit($id = null)
    {
        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos modificar un registro nuevo.

            //Hacemos una consulta contra la api para modificar el registro del modelo
            $modelResponse      = $this->model->setRecord($this->request->data);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            switch ($modelResponse->getType()){
                case 1:
                    //correcto
                    $this->Flash->set(__('El registro se editó correctamente.'), ['element' => 'success']);
                    return $this->redirect(['action' => 'index']);
                break;
                case -1:
                    //error
                    $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                break;
            }

        }
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();

        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Tags          = $foreings['Tags'];
        $TagsOptions   = [];
        foreach ($Tags->getList(true)->getDatas() as $datalist){            
            array_push($TagsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
              
        }

        $Albums          = $foreings['Albums'];
        $AlbumsOptions   = [];
        foreach ($Albums->getList(true)->getDatas() as $datalist){            
            array_push($AlbumsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
              
        }
       
        $this->set('TagsOptions',$TagsOptions);
        $this->set('AlbumsOptions',$AlbumsOptions);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }
    
    
    /**
     * View método para mostrar un registro que viene como
     * parámetro
     *
     * @param string|null $id ModelApi id vía get.
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function view($id = null)
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Tags          = $foreings['Tags'];
        $TagsOptions   = [];
        foreach ($Tags->getList(true)->getDatas() as $datalist){            
            array_push($TagsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
              
        }

        $Albums          = $foreings['Albums'];
        $AlbumsOptions   = [];
        foreach ($Tags->getList(true)->getDatas() as $datalist){            
            array_push($AlbumsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
              
        }
        
        $this->set('tag_name',$Tags->getRecord($data->tag_id)->getDataFirst()->name);
        $this->set('album_name',$Albums->getRecord($data->album_id)->getDataFirst()->name);
        $this->set('TagsOptions',$TagsOptions);
        $this->set('AlbumsOptions',$AlbumsOptions);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }
}
