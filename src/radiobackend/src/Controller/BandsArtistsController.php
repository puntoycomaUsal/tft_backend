<?php
namespace App\Controller;

use App\Controller\AppApisController;
use App\Modelapi\BandsArtistApi;

/**
 * BandsArtists Controller
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class BandsArtistsController extends AppApisController
{

    /**
     * Initialize metodo para inicializar el controlador
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function initialize()
    {
        parent::setConfig(new BandsArtistApi());
    }

    /**
     * Index método usado para mostrar un listado
     * de registros de la entidad
     *
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function index()
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getList();

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos los datos a mostrar
        $datas              = $modelResponse->getDatas();
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Artists = $foreings['Artists'];
        $Bands   = $foreings['Bands'];
        $Roles   = $foreings['Roles'];
       
        $this->set('Artists',$Artists);
        $this->set('Bands',$Bands);
        $this->set('Roles',$Roles);
        $this->set(compact('datas'));
        $this->set('_serialize', ['datas']);
        $this->viewBuilder()->layout('default');

    }



    /**
     * Add método para añadir registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se creó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function add()
    {
        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos crear un registro nuevo.

            //Hacemos una consulta contra la api para crear el registro del modelo
            $modelResponse      = $this->model->setRecord($this->request->data);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            switch ($modelResponse->getType()){
                case 1:
                    //correcto
                    $this->Flash->set(__('El registro se guardó correctamente.'), ['element' => 'success']);
                    return $this->redirect(['action' => 'index']);
                break;
                case -1:
                    //error
                    $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                break;
            }

        }
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();

        $Bands              = $foreings['Bands'];
        $BandsOptions       = [];
        foreach ($Bands->getList(true)->getDatas() as $data){            
            array_push($BandsOptions,['value' => $data['id'], 'text' => $data['caption']]);
        }

        $Artists              = $foreings['Artists'];
        $ArtistsOptions       = [];
        foreach ($Artists->getList(true)->getDatas() as $data){            
            array_push($ArtistsOptions,['value' => $data['id'], 'text' => $data['caption']]);
        }

        $Roles              = $foreings['Roles'];
        $RolesOptions       = [];
        foreach ($Roles->getList(true)->getDatas() as $data){            
            array_push($RolesOptions,['value' => $data['id'], 'text' => $data['caption']]);
        }

        $this->set('BandsOptions',$BandsOptions);
        $this->set('ArtistsOptions',$ArtistsOptions);
        $this->set('RolesOptions',$RolesOptions);
        $this->viewBuilder()->layout('default');
    }



    /**
     * Add método para editar registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se modificó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function edit($id = null)
    {
        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos modificar un registro nuevo.

            //Hacemos una consulta contra la api para modificar el registro del modelo
            $modelResponse      = $this->model->setRecord($this->request->data);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            switch ($modelResponse->getType()){
                case 1:
                    //correcto
                    $this->Flash->set(__('El registro se editó correctamente.'), ['element' => 'success']);
                    return $this->redirect(['action' => 'index']);
                break;
                case -1:
                    //error
                    $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                break;
            }

        }
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();

        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Bands          = $foreings['Bands'];
        $BandsOptions   = [];
        foreach ($Bands->getList(true)->getDatas() as $datalist){            
            array_push($BandsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
        }

        $Artists          = $foreings['Artists'];
        $ArtistsOptions   = [];
        foreach ($Artists->getList(true)->getDatas() as $datalist){            
            array_push($ArtistsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
        }

        $Roles          = $foreings['Roles'];
        $RolesOptions   = [];
        foreach ($Roles->getList(true)->getDatas() as $datalist){            
            array_push($RolesOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
        }

        $this->set('BandsOptions',$BandsOptions);
        $this->set('ArtistsOptions',$ArtistsOptions);
        $this->set('RolesOptions',$RolesOptions);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }
    
    
    /**
     * View método para mostrar un registro que viene como
     * parámetro
     *
     * @param string|null $id ModelApi id vía get.
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function view($id = null)
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();

        $Bands          = $foreings['Bands'];
        $BandsOptions   = [];
        foreach ($Bands->getList(true)->getDatas() as $datalist){            
            array_push($BandsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
        }

        $Artists          = $foreings['Artists'];
        $ArtistsOptions   = [];
        foreach ($Artists->getList(true)->getDatas() as $datalist){            
            array_push($ArtistsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
        }

        $Roles          = $foreings['Roles'];
        $RolesOptions   = [];
        foreach ($Roles->getList(true)->getDatas() as $datalist){            
            array_push($RolesOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
        }
        
        $this->set('artist_name',$Artists->getRecord($data->artist_id)->getDataFirst()->first_name);
        $this->set('band_name',$Bands->getRecord($data->band_id)->getDataFirst()->name);
        $this->set('role_name',$Roles->getRecord($data->role_id)->getDataFirst()->name);
        $this->set('BandsOptions',$BandsOptions);
        $this->set('ArtistsOptions',$ArtistsOptions);
        $this->set('RolesOptions',$RolesOptions);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }
}
