<?php
namespace App\Controller;

use App\Controller\AppApisController;
use App\Modelapi\ArtistApi;

/**
 * Artists Controller
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
class ArtistsController extends AppApisController
{

    /**
     * Initialize metodo para inicializar el controlador
     *
     * @return void
     * @author Jesús Manuel Nieto Carracedo [Glifing S.L.]
     * @copyright Glifing S.L.
     * @version 1.0
     */
    public function initialize()
    {
        parent::setConfig(new ArtistApi());
    }

}
