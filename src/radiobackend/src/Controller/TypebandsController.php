<?php
namespace App\Controller;

use App\Controller\AppApisController;
use App\Modelapi\TypebandApi;

/**
 * Typebands Controller
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class TypebandsController extends AppApisController
{

    /**
     * Initialize metodo para inicializar el controlador
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function initialize()
    {
        parent::setConfig(new TypebandApi());
    }

}
