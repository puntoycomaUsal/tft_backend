<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Dataobject\ModelResponse;
use App\Modelapi\ModelApi;

/**
 * AppApis Controller será el controlador base para los
 * controladores basados en Api
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
class AppApisController extends AppController
{

    //Cargaremos aquí el modelo del controlador
    protected $model;

    /**
     * setConfig metodo para inicializar el controlador
     *
     * @param App\Modelapi\ModelApi objeto de tipo modelo de datos Api
     * @return void
     * @author Jesús Manuel Nieto Carracedo [Glifing S.L.]
     * @copyright Glifing S.L.
     * @version 1.0
     */
    public function setConfig($model)
    {
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->model = $model;
    }

    /**
     * Index método usado para mostrar un listado
     * de registros de la entidad
     *
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function index()
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getList();

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos los datos a mostrar
        $datas              = $modelResponse->getDatas();
        
        $this->set(compact('datas'));
        $this->set('_serialize', ['datas']);
        $this->viewBuilder()->layout('default');

    }

    /**
     * View método para mostrar un registro que viene como
     * parámetro
     *
     * @param string|null $id ModelApi id vía get.
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function view($id = null)
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }

    /**
     * Add método para añadir registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se creó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function add()
    {
        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos crear un registro nuevo.

            //Hacemos una consulta contra la api para crear el registro del modelo
            $modelResponse      = $this->model->setRecord($this->request->data);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            switch ($modelResponse->getType()){
                case 1:
                    //correcto
                    $this->Flash->set(__('El registro se guardó correctamente.'), ['element' => 'success']);
                    return $this->redirect(['action' => 'index']);
                break;
                case -1:
                    //error
                    $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                break;
            }

        }
        $this->viewBuilder()->layout('default');
    }

    /**
     * Add método para editar registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se modificó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function edit($id = null)
    {
        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos modificar un registro nuevo.

            //Hacemos una consulta contra la api para modificar el registro del modelo
            $modelResponse      = $this->model->setRecord($this->request->data);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            switch ($modelResponse->getType()){
                case 1:
                    //correcto
                    $this->Flash->set(__('El registro se editó correctamente.'), ['element' => 'success']);
                    return $this->redirect(['action' => 'index']);
                break;
                case -1:
                    //error
                    $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                break;
            }

        }
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }

    /**
     * Delete método para eliminar un registro.
     *
     * @param string|null $id Task id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function delete($id = null)
    {
        //Lo que haremos es comprobar que que sea una petición llegada al método correspondiente.
        $this->request->allowMethod(['post', 'delete']);

        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->deleteRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        switch ($modelResponse->getType()){
            case 1:
                //correcto
                $this->Flash->set(__('El registro se eliminó correctamente.'), ['element' => 'success']);
            break;
            case -1:
                //error
                $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
            break;
        }

        return $this->redirect(['action' => 'index']);
    }
}
