<?php
namespace App\Controller;

use App\Controller\AppApisController;
use App\Modelapi\ConfigurationApi;

/**
 * Configurations Controller
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class ConfigurationsController extends AppApisController
{

    /**
     * Initialize metodo para inicializar el controlador
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function initialize()
    {
        parent::setConfig(new ConfigurationApi());
    }

}
