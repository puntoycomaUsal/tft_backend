<?php
namespace App\Controller;

use App\Controller\AppApisController;
use App\Modelapi\ItemApi;
use App\Configuration\ConfigurationEndpoint;

/**
 * Items Controller
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class ItemsController extends AppApisController
{

    /**
     * Initialize metodo para inicializar el controlador
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function initialize()
    {
        parent::setConfig(new ItemApi());
    }

    /**
     * Index método usado para mostrar un listado
     * de registros de la entidad
     *
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function index()
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getList();

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos los datos a mostrar
        $datas              = $modelResponse->getDatas();
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Typeitems          = $foreings['Typeitems'];
        $Bands              = $foreings['Bands'];
       
        $this->set('Typeitems',$Typeitems);
        $this->set('Bands',$Bands);
        $this->set(compact('datas'));
        $this->set('_serialize', ['datas']);
        $this->viewBuilder()->layout('default');

    }



    /**
     * Add método para añadir registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se creó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function add()
    {

        $resources          = ConfigurationEndpoint::getRecord();

        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Typeitems          = $foreings['Typeitems'];
        $TypeitemsOptions   = [];
        foreach ($Typeitems->getList(true)->getDatas() as $data){            
            array_push($TypeitemsOptions,['value' => $data['id'], 'text' => $data['caption']]);
        }

        $Bands              = $foreings['Bands'];
        $BandsOptions       = [];
        foreach ($Bands->getList(true)->getDatas() as $data){            
            array_push($BandsOptions,['value' => $data['id'], 'text' => $data['caption']]);
        }

        if ($this->request->is('post')) {
            // Si es una petición post indica que pretendemos crear un registro nuevo.

            $file               = $this->request->data['url'];

            if( $file['error'] == 0  &&  $file['size'] > 0)
            {
                $typeitem_id = array_search($this->request->data['typeitem_id'], array_column($TypeitemsOptions,'value'));
                $typeitem_name = $TypeitemsOptions[$typeitem_id]['text'];
                $destination              = $resources->getDataFirst()->pathResources.DS.$typeitem_name.DS.$file['name'];
                if (!file_exists($destination))
                {
                    if (move_uploaded_file($file['tmp_name'], $destination))
                    {
                        //Hacemos una consulta contra la api para crear el registro del modelo, en este caso se hace
                        //lo último solo si el fichero se ha podido guardar.
                        $this->request->data['url'] = '/'.$typeitem_name.'/'.$file['name'];

                        $modelResponse      = $this->model->setRecord($this->request->data);

                        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
                        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
                        switch ($modelResponse->getType()){
                            case 1:
                                //correcto
                                $this->Flash->set(__('El registro se guardó correctamente.'), ['element' => 'success']);
                                return $this->redirect(['action' => 'index']);
                            break;
                            case -1:
                                //error
                                $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                            break;
                        }
                    } else {
                        $this->Flash->set(__('El fichero no pudo guardarse en disco revisar ubicación y permisos'), ['element' => 'error']);
                    }
                } else {
                    $this->Flash->set(__('El fichero ya existe.'), ['element' => 'error']);
                }
            }else{
                 $this->Flash->set(__('Error al intentar subir el archivo'), ['element' => 'error']);
             }
        }

        $this->set('TypeitemsOptions',$TypeitemsOptions);
        $this->set('BandsOptions',$BandsOptions);
        $this->viewBuilder()->layout('default');
    }



    /**
     * Add método para editar registros
     *
     * @return \Cake\Network\Response|void Redirige a index si se modificó correctamente, vuelve al formulario
     * en caso de error.
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function edit($id = null)
        {
            $resources          = ConfigurationEndpoint::getRecord();

            //Hacemos una consulta contra la api para traer los registros del modelo
            $modelResponse      = $this->model->getRecord($id);

            //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
            //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
            if ($modelResponse->getType() == -1){
                $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
            }

            //Traemos el dato a mostrar
            $data               = $modelResponse->getDataFirst();

            //Colección de datos de clave foránea
            $foreings           = $this->model->getForeigns();
            
            $Typeitems          = $foreings['Typeitems'];
            $TypeitemsOptions   = [];
            foreach ($Typeitems->getList(true)->getDatas() as $datalist){            
                array_push($TypeitemsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
                  
            }

            $Bands          = $foreings['Bands'];
            $BandsOptions   = [];
            foreach ($Bands->getList(true)->getDatas() as $datalist){            
                array_push($BandsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
            }

            if ($this->request->is('post')) {
                
                $file = $this->request->data['url'];

                if(!empty($file['name'])){

                    $typeitem_id   = array_search($this->request->data['typeitem_id'], array_column($TypeitemsOptions,'value'));
                    
                    $typeitem_name = $TypeitemsOptions[$typeitem_id]['text'];
                    
                    $destination   = $resources->getDataFirst()->pathResources.DS.$typeitem_name.DS.$file['name'];

                        if (!file_exists($destination))
                        {
                            if (move_uploaded_file($file['tmp_name'], $destination))
                            {
                                //Hacemos una consulta contra la api para crear el registro del modelo, en este caso se hace
                                //lo último solo si el fichero se ha podido guardar.
                                $this->request->data['url'] = '/'.$typeitem_name.'/'.$file['name'];

                                $existsUrl     = $this->isExistsURL($data->url);

                                if ($existsUrl < 2) {
                                    $fileName  = $resources->getDataFirst()->pathResources.$data->url;
                                    unlink($fileName);
                                }

                            } else {
                                $this->Flash->set(__('Error al intentar subir el archivo'), ['element' => 'error']);
                            }
                        }else{
                             $this->Flash->set(__('El fichero ya existe.'), ['element' => 'error']);
                         }
                } else {
                    $this->request->data['url'] = $data->url;
                }

                $modelResponse      = $this->model->setRecord($this->request->data);

                //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
                //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
                switch ($modelResponse->getType()){
                    case 1:
                        //correcto
                        $this->Flash->set(__('El registro se editó correctamente.'), ['element' => 'success']);
                        return $this->redirect(['action' => 'index']);
                    break;
                    case -1:
                        //error
                        $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
                    break;
                }
            }
           
            $this->set('TypeitemsOptions',$TypeitemsOptions);
            $this->set('BandsOptions',$BandsOptions);
            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
            $this->viewBuilder()->layout('default');
        }

    /**
     * View método para mostrar un registro que viene como
     * parámetro
     *
     * @param string|null $id ModelApi id vía get.
     * @return \Cake\Network\Response|null
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function view($id = null)
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        if ($modelResponse->getType() == -1){
            $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
        }

        //Traemos el dato a mostrar
        $data               = $modelResponse->getDataFirst();
        
        //Colección de datos de clave foránea
        $foreings           = $this->model->getForeigns();
        
        $Typeitems          = $foreings['Typeitems'];
        $TypeitemsOptions   = [];
        foreach ($Typeitems->getList(true)->getDatas() as $datalist){            
            array_push($TypeitemsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
              
        }

        $Bands          = $foreings['Bands'];
        $BandsOptions   = [];
        foreach ($Bands->getList(true)->getDatas() as $datalist){            
            array_push($BandsOptions,['value' => $datalist['id'], 'text' => $datalist['caption']]);
              
        }
        
        $this->set('typeitem_name',$Typeitems->getRecord($data->typeitem_id)->getDataFirst()->name);
        $this->set('band_name',$Bands->getRecord($data->band_id)->getDataFirst()->name);
        $this->set('TypeitemsOptions',$TypeitemsOptions);
        $this->set('BandsOptions',$BandsOptions);
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        $this->viewBuilder()->layout('default');
    }

    /**
     * Delete método para eliminar un registro.
     *
     * @param string|null $id Task id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 3.0
     */
    public function delete($id = null)
    {
        $resources          = ConfigurationEndpoint::getRecord();

        //Lo que haremos es comprobar que que sea una petición llegada al método correspondiente.
        $this->request->allowMethod(['post', 'delete']);

        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse = $this->model->getRecord($id);
        
        //Traemos el dato a mostrar
        $data          = $modelResponse->getDataFirst();
        
        $existsUrl     = $this->isExistsURL($data->url);

        if ($existsUrl < 2) {
            $fileName  = $resources->getDataFirst()->pathResources.$data->url;
            unlink($fileName);
        }

        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->deleteRecord($id);

        //Se comprueba y carga si existe algún tipo de error para mostrar el mensaje
        //Aunque exista error, siempre se devolverá un datas con un array vacío como mínmimo.
        switch ($modelResponse->getType()){
            case 1:
                //correcto
                $this->Flash->set(__('El registro se eliminó correctamente.'), ['element' => 'success']);
            break;
            case -1:
                //error
                $this->Flash->set($modelResponse->getMessage(), ['element' => 'error']);
            break;
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * isExistsURL para comprobar que no existan más items con la misma url.
     *
     * @return \Cake\Network\Response|void Redirige a index si se modificó correctamente, vuelve al formulario
     * en caso de error.
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function isExistsURL($url)
    {
        //Hacemos una consulta contra la api para traer los registros del modelo
        $modelResponse      = $this->model->getList();

        //Traemos los datos a mostrar
        $datas              = $modelResponse->getDatas();

        $numberExists = 0;

        foreach ($datas as $data){
            if (strcmp($data->url, $url) == 0){
                $numberExists++;
            }
        }
        return $numberExists;
    }
}
