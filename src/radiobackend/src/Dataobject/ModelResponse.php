<?php
namespace App\Dataobject;

/**
 * ModelResponse
 *
 * Clase de datos que usaremos para crear objetos de tipo respuesta
 * entre los modelos y los controladores.
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */


class ModelResponse
{
    private $type;
    private $message;
    private $datas;

    /**
    * __construct de clase con el nombre de la entidad de la
    * base de datos.
    *
    * @param int tipo de respuesta, 1 ok, -1 error.
    * @param string message que indica información de la respuesta
    * @param array datas que contendrá los datos consultados.
    * @return void
    * @author Jesús Manuel Nieto Carracedo
    * @copyright punto&coma
    * @version 1.0
    */
   function __construct($type,$message,$datas) {
       $this->type      = $type;
       $this->message   = $message;
       $this->datas     = $datas;
   }

   /**
    * getType  devuelve el tippo de dato
    *
    * @return int tipo de respuesta, 1 ok, -1 error.
    * @author Jesús Manuel Nieto Carracedo
    * @copyright punto&coma
    * @version 1.0
    */
    public function getType() {
        return $this->type;
    }

    /**
    * getMessage  devuelve el mensaje asociado
    *
    * @return message string que indica información de la respuesta
    * @author Jesús Manuel Nieto Carracedo
    * @copyright punto&coma
    * @version 1.0
    */
    public function getMessage() {
        return $this->message;
    }

     /**
    * getDatas  devuelve los registros del modelo
    *
    * @return getDatas|null array que contendrá los datos consultados, en caso de error devolverá un valor nulo
    * @author Jesús Manuel Nieto Carracedo
    * @copyright punto&coma
    * @version 1.0
    */
    public function getDatas() {
        try {
            return $this->datas;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
    * getData  devuelve el primer registro de datos, es útil o se creó para el caso de traer
    * o tener un solo registro, cuando hacermos un get/$id por ejemplo.
    *
    * @param int $id el índice que queremos recuperar
    * @return getData|null el elemento solicitado, en caso de no existir, devuelve un nulo
    * @author Jesús Manuel Nieto Carracedo
    * @copyright punto&coma
    * @version 1.0
    */
    public function getDataFirst() {
        try {
            return $this->datas[0];
        } catch (\Exception $e) {
            return null;
        }
    }


}
