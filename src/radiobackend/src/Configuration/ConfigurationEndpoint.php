<?php
namespace App\Configuration;

use Cake\Network\Http\Client;
use Cake\Core\Configure;
use App\Dataobject\ModelResponse;

/**
 * ConfigurationApi
 *
 * Clase de configuración de Api que usaremos para poder acceder a la tabla
 * de configuración de pone a disposición la Api.
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */


class ConfigurationEndpoint
{
    /**
     * getRecord Traerá el regostro de configuración de la Api
     *
     * @return App\Dataobject\ModelResponse objeto de tipo respuesta del modelo
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public static function getRecord (){
        //Creamos variables
        $http           = new Client();

        //Llamamos con parámetros a la función que nos traerá el registro del modelo
        //recordar que es una petición get, luego el parámetro irá en la zona get.
        try {
            $msg            = $http->get( Configure::read('endpointConfigurationApi').'/'.strval(1), [], ['type' => 'json']);
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, $body->data);
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, $body->data);
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return new ModelResponse(-1, $message, array());
                break;
        }

    }
    
    /**
     * getModelEndpoints Para devolver los modelos con sus endpoints
     *
     * @return Array objeto de tipo diccionario que devolverá parekas model => endpoint
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public static function getModelEndpoints (){
        //Creamos variables
        $http           = new Client();

        //Llamamos con parámetros a la función que nos traerá el registro del modelo
        //recordar que es una petición get, luego el parámetro irá en la zona get.
        try {
            $msg            = $http->get(Configure::read('endpointConfigurationApi').'/'.strval(1), [], ['type' => 'json']);
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return array();
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                
                $modelResponse      = new ModelResponse(intval($body->type),$body->message, $body->data);
                
                $response           = array();
                
                foreach (json_decode($modelResponse->getDataFirst()->endpoints) as $endpoints){
                    foreach ($endpoints as $model => $endpoint){
                        $response[$model] = Configure::read('endpointApi').$endpoint;
                    }
                }
                
                return $response;
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return array();
                break;
        }

    }

}
