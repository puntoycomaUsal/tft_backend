<?php
namespace App\ModelApi;

use Cake\Network\Http\Client;
use App\Dataobject\ModelResponse;
use App\Configuration\ConfigurationEndpoint;

/**
 * ModelApi
 *
 * Clase base que usaremos para generar los modelos basados en
 * nuestra Api de radio blog punto & coma.
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */


class ModelApi
{

    protected $name;            //Nombre de la clase
    protected $urlapi;          //Dirección a la cual apunta el endpoint de la entidad
    protected $modelEndpoints;  //Diccionario de modelos y sus endpoints
    protected $foreings;        //Array de ModelApi que contiene las claves foráneas de la entidad (_id)
    protected $caption;         //Será el nombre del campo que se mostrará en los desplegables para seleccionar un registro.

    /**
     * setConfig de clase, recibe el nombre de la entidad
     * para comunicar con la base de datos a través de la api
     *
     * @param string    $name Cadena con el nombre de la entidad
     * @param array()   $foreings Contendrá los objetos de tipo modelo que contienen clave foránea
     * @param string    $caption será el campo que se seleccionará para mostrar en los desplegables, por defecto el id.
     * @return void
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    function setConfig($name, $foreings = [], $caption = 'id') {
        $this->name             = $name;
        $this->modelEndpoints   = ConfigurationEndpoint::getModelEndpoints();
        $this->urlapi           = $this->modelEndpoints[$this->name];
        $this->caption          = $caption;
        $this->foreings         = $foreings;
    }

    /**
     * setRecord Se usará para crear/editar un nuevo registro
     *
     * @param array  $data Campos a insertar/editar.
     * @return App\Dataobject\ModelResponse objeto de tipo respuesta del modelo
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function setRecord ($data){
        //Creamos variables
        $http           = new Client();

        //Llamamos con parámetros a la función que creará/editará el registro del modelo
        //recordar que es una petición post, luego los parámetros irá en la zona post.
        try {
            if (!array_key_exists('id',$data)){
                //Crear registro
                $msg            = $http->post( $this->urlapi, $data, ['type' => 'json']);
            } else {
                //Editar registro
                $msg            = $http->put( $this->urlapi.'/'.$data['id'], $data, ['type' => 'json']);
            }
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            case 201:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return new ModelResponse(-1, $message, array());
                break;
        }
    }

    /**
     * getRecord Traerá un registro especificado en el id
     *
     * @param int $id será el id a buscar en el modelo
     * @return App\Dataobject\ModelResponse objeto de tipo respuesta del modelo
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function getRecord ($id){
        //Creamos variables
        $http           = new Client();

        //Llamamos con parámetros a la función que nos traerá el registro del modelo
        //recordar que es una petición get, luego el parámetro irá en la zona get.
        try {
            $msg            = $http->get( $this->urlapi.'/'.strval($id), [], ['type' => 'json']);
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, $body->data);
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, $body->data);
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return new ModelResponse(-1, $message, array());
                break;
        }

    }

    /**
     * deleteRecord Eliminará un registro especificado en el id
     *
     * @param int $id será el id a eliminar
     * @return App\Dataobject\ModelResponse objeto de tipo respuesta del modelo
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function deleteRecord($id){
        //Creamos variables
        $http           = new Client();

        //Llamamos con parámetros a la función que nos eliminará el registro del modelo
        //recordar que es una petición delete, con parámetro get luego el parámetro irá en la zona get.
        try {
            $msg            = $http->delete( $this->urlapi.'/'.strval($id), [], ['type' => 'json']);
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return new ModelResponse(-1, $message, array());
                break;
        }
    }

    /**
     * getList Traerá un listado con todos los registros.
     *
     * @param $getCaprionList cuando sea cierto, el listado solo devolvera un diccionario
     * del tipo (id,caption) se usará para los listados desplegables como una clave foránea _id
     * @return App\Dataobject\ModelResponse objeto de tipo respuesta del modelo
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function getList($getCaprionList = false){
        //Creamos variables
        $http           = new Client();

        //Llamamos sin parámetros a la función que nos traerá los registros del modelo
        try {
            $msg            = $http->get( $this->urlapi, [], ['type' => 'json']);
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, $body->data);
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                if ($getCaprionList){
                    return new ModelResponse(intval($body->type),$body->message, $this->getCaptionList($body->data));
                } else {
                    return new ModelResponse(intval($body->type),$body->message, $body->data);
                }
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return new ModelResponse(-1, $message, array());
                break;
        }
    }
    
    /**
     * getCaptionList Generará un diccionario con formato ($id, $caption) que usaremos para los
     * listados de tipo desplegable.
     *
     * @param $datas será una variable en formato json 
     * del tipo (id,caption) se usará para los listados desplegables como una clave foránea _id
     * @return Array diccionario que contendrá el id y el campo escrito en caption
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    private function getCaptionList($datas){
        $responses = [];
        
        foreach ($datas as $data ){
            $response = [];
            foreach ($data as $field => $value){
                if ($field == 'id') {
                  $response['id']     = $value;  
                }
                if ($field == $this->caption) {
                  $response['caption'] = $value;  
                }
            }
            array_push($responses, $response);
        }
        
        return $responses;
    }

    /**
     * getForeigns Devuevle array con los modelos de clave foránea para poder iterar de
     * forma recursiva y llegar a sus campos relacionados.
     *
     * @return Array de modelApi de entidades foráneas
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    public function getForeigns(){    
        return $this->foreings;
    }
}
