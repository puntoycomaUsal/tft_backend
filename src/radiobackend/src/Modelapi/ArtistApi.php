<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\ModelApi\TypebandApi;

/**
 * Artist Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Jesús Manuel Nieto Carracedo
 * @copyright punto&coma
 * @version 1.0
 */
class ArtistApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Jesús Manuel Nieto Carracedo
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        
        parent::setConfig('Artists',$foreigns,'first_name');
    }
}
