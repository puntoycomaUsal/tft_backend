<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\Modelapi\AlbumApi;
use App\Modelapi\TagApi;

/**
 * TagsAlbum Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class TagsAlbumApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        $foreigns['Albums']  = new AlbumApi();
        $foreigns['Tags']   = new TagApi();
        
        parent::setConfig('TagsAlbums',$foreigns,'id');
    }
}
