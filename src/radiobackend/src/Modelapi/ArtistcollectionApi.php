<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use Cake\Network\Http\Client;
use App\Dataobject\ModelResponse;

/**
 * Artistcollection Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class ArtistcollectionApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        
        parent::setConfig('Artistcollections',$foreigns,'name');
    }


    /**
     * setRecord Se usará para crear/editar un nuevo registro
     *
     * @param array  $data Campos a insertar/editar.
     * @return App\Dataobject\ModelResponse objeto de tipo respuesta del modelo
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    public function setRecord ($data){
        //Creamos variables
        $http           = new Client();

        //Traemos la lista completa y cogemos el ultimo array para saber si es un id nuevo.
        $modelResponse      = $this->getList();
        $datas              = $modelResponse->getDatas();
        $lastData = end($datas);

        try {
            //Comparamos si el valor del id que se envia es mayor al ultimo id de la lista
            //Si es mayor quiere decir que es un nuevo valor.
            if ($data['_id'] > $lastData->_id){
                //Crear registro
                $msg            = $http->post( $this->urlapi, $data, ['type' => 'json']);

            } else {
                //Editar registro
                $msg            = $http->put( $this->urlapi.'/'.$data['_id'], $data, ['type' => 'json']);
            }
        } catch (\Exception $e) {
            $message            = "Hubo un error intentando conectar con el endpoint ".$this->name.". Mensaje de Error: ".$e->getMessage();
            return new ModelResponse(-1, $message, array());
        }

        switch (intval($msg->code)){
            case 500:
                //Error
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            case 200:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            case 201:
                //Correcto
                $body               = json_decode($msg->body());
                return new ModelResponse(intval($body->type),$body->message, array());
                break;
            default:
                //Para cualquier otro código de error tendremos un mensaje desconocido.
                $message            = "Hubo un error desconocido código ".$msg->code;
                return new ModelResponse(-1, $message, array());
                break;
        }
    }
}
