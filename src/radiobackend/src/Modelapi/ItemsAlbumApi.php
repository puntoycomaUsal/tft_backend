<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\Modelapi\AlbumApi;
use App\Modelapi\ItemApi;

/**
 * ItemsAlbum Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class ItemsAlbumApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        $foreigns['Albums']    = new AlbumApi();
        $foreigns['Items']     = new ItemApi();
        $foreigns['Typeitems'] = new TypeitemApi();
        
        parent::setConfig('ItemsAlbums',$foreigns,'id');
    }
}
