<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\Modelapi\ArtistApi;
use App\Modelapi\BandApi;
use App\Modelapi\RoleApi;

/**
 * BandsArtist Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class BandsArtistApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        $foreigns['Artists']    = new ArtistApi();
        $foreigns['Bands']      = new BandApi();
        $foreigns['Roles']      = new RoleApi();
        
        
        parent::setConfig('BandsArtists',$foreigns,'name');
    }
}
