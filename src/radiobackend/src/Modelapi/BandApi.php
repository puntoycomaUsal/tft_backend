<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\Modelapi\TypebandApi;

/**
 * Band Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class BandApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        $foreigns['Typebands']  = new TypebandApi();
        
        parent::setConfig('Bands',$foreigns,'name');
    }
}
