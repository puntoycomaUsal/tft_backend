<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\Modelapi\TypeitemApi;
use App\Modelapi\BandApi;

/**
 * Item Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class ItemApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        $foreigns['Typeitems']  = new TypeitemApi();
        $foreigns['Bands']      = new BandApi();
        
        parent::setConfig('Items',$foreigns,'name');
    }
}
