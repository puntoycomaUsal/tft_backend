<?php
namespace App\ModelApi;

use App\Modelapi\ModelApi;
use App\Modelapi\BandApi;
use App\Modelapi\ItemApi;

/**
 * ItemsBand Modelapi
 *
 * Modelo que deberemos acoplar
 *
 * @author Maritza Bravo Vargas
 * @copyright punto&coma
 * @version 1.0
 */
class ItemsBandApi extends ModelApi
{
    /**
     * __construct de clase con el nombre de la entidad de la
     * base de datos.
     *
     * @return void
     * @author Maritza Bravo Vargas
     * @copyright punto&coma
     * @version 1.0
     */
    function __construct() {
        $foreigns = [];
        $foreigns['Bands']     = new BandApi();
        $foreigns['Items']     = new ItemApi();
        $foreigns['Typeitems'] = new TypeitemApi();
        
        parent::setConfig('ItemsBands',$foreigns,'id');
    }
}
